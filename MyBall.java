import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.util.*;

class MyBall {
	Stage stage;
	double width, height; // 画面サイズ
	double x, y; // 中心座標
	double size; // 半径
	double movX, movY; // 速度
	Color color;
	double grav = 0.3; // 重力
	double coef = 0.9; // 壁との反発係数
	double mass = 1.0; // 質量(密度)
	boolean colliCeil = true; // 天井で跳ね返るか
	boolean delFlag = false;
	MyBall(Stage st, double x, double y, double s, double mx, double my, Color col){
		stage = st;
		width = stage.getWidth();
		height = stage.getHeight();
		this.x = x;
		this.y = y;
		size = s;
		movX = mx;
		movY = my;
		color = col;
	}
	MyBall(Stage st, double x, double y, double s, double mx, double my, Color col, double g, double k, double m){
		stage = st;
		width = stage.getWidth();
		height = stage.getHeight();
		this.x = x;
		this.y = y;
		size = s;
		movX = mx;
		movY = my;
		color = col;
		grav = g;
		coef = k;
		mass = m;
	}

	double getX() { return x; }
	double getY() { return y; }
	double getMovX() { return movX; }
	double getMovY() { return movY; }
	double getSize() { return size; }
	boolean getDelFlag() { return delFlag; }
	void setX(double x) { this.x = x; }
	void setY(double y) { this.y = y; }
	void setXY(double x, double y) { this.x = x; this.y = y; }
	void setGrav(double g) { grav = g; }
	void setCoef(double k) { coef = k; }
	void setMass(double m) { mass = m; }
	void setMovX(double movX) { this.movX = movX; }
	void setMovY(double movY) { this.movY = movY; }
	void setMovXY(double movX, double movY) { this.movX = movX; this.movY = movY; }
	void setSize(double s) { size = s; }
	void setColor(Color col) { color = col; }
	void setDelFlag(){ delFlag = true; }

	double getSpeed(){
		return Math.sqrt(movX*movX + movY*movY);
	}
	double getDir(){
		return Math.atan(movY/movX);
	}

	void draw(Graphics g){
		Color backup = g.getColor();
		g.setColor(color);
		g.fillOval((int)(x - size), (int)(y - size), (int)size*2, (int)size*2);
		g.setColor(backup);
	}

	void next(){
		x += movX;
		movY += grav;
		y += movY;
		colliStage();
	}

	void colliStage(){
		if(x-size < 0 && movX < 0){
			x = 0+size;
			movX = -movX * coef;
		} else if(x+size > width && movX > 0){
			x = width-size;
			movX = -movX * coef;
		}
		if(y+size > height && movY > 0){
			y = height-size;
			movY = -movY * coef;
		} else if(y-size < 0 && movY < 0 && colliCeil){
			y = 0+size;
			movY = -movY * coef;
		}
	}
}

class ListMyBall{
	List<MyBall> list;
	Color col1 = Color.black;
	Color col2 = Color.red;
	int i,j;
	Stage stage;
	double width, height;

	double coefBall = 0.95;

	ListMyBall(Stage st){
		stage = st;
		width = st.getWidth();
		height = st.getHeight();
		list = new ArrayList<MyBall>();
//		addBallDir(width/2,height/2,50,Math.PI*300/180,10,col1);
//		addBall(0,0,15,2,2,col1);
//		addBall(width-10,height-10,10,-2,-14,col1);
	}
	public int getBallNum(){ return list.size(); }

	void addBall(double x, double y, double s, double mx, double my, Color col){
		list.add(new MyBall(stage,x,y,s,mx,my,col));
	}
	void addBall(double x, double y, double s, double mx, double my, Color col, double g, double k, double m){
		list.add(new MyBall(stage,x,y,s,mx,my,col,g,k,m));
	}
	void addBallDir(double x, double y, double s, double dir, double speed, Color col){
		double mx = speed * Math.cos(dir);
		double my = speed * Math.sin(dir);
		list.add(new MyBall(stage,x,y,s,mx,my,col));
	}
	void draw(Graphics g){
		for (i=getBallNum()-1; i>=0 ; i--){
			list.get(i).draw(g);
		}
	}
	void next(){
		for (i=getBallNum()-1; i>=0; i--){
			list.get(i).next();
			// Ball同士の衝突判定
			for(j=i-1; j>=0; j--){
				colliBall(list.get(i), list.get(j));
			}
		}
	}

	void colliBall(MyBall b1, MyBall b2){
		if(Collision.circle(b1.x, b1.y, b1.size, b2.x, b2.y, b2.size)){
			double relativeX, relativeY; // b1-b2間の相対速度ベクトル
			double inx, iny; // 衝突面に垂直な単位ベクトル

			relativeX = b1.movX - b2.movX;
			relativeY = b1.movY - b2.movY;

			inx = (b2.x-b1.x)/Math.sqrt((b2.x-b1.x)*(b2.x-b1.x) + (b2.y-b1.y)*(b2.y-b1.y));
			iny = (b2.y-b1.y)/Math.sqrt((b2.x-b1.x)*(b2.x-b1.x) + (b2.y-b1.y)*(b2.y-b1.y));

			if(relativeX*inx + relativeY*iny > 0){
				double m1, m2; // 質量
				// 衝突前
//				double v1sp, v2sp; // 速度
				double itx, ity; // 衝突面に平行な単位ベクトル
				double vn1sp; // 衝突面に垂直な速度ベクトル
				double vn2sp;
				double vt1x, vt1y, vt1sp; // 衝突面に平行な速度ベクトル
				double vt2x, vt2y, vt2sp;
				// 衝突後
				double outvn1x, outvn1y, outvn1sp; // 衝突面に垂直な速度ベクトル
				double outvn2x, outvn2y, outvn2sp;
				double outv1x, outv1y; // 衝突後の速度ベクトル
				double outv2x, outv2y;

				m1 = b1.mass * (4/3) * Math.PI * b1.size * b1.size * b1.size;
				m2 = b2.mass * (4/3) * Math.PI * b2.size * b2.size * b2.size;

//				v1sp = Math.sqrt( b1.movX*b1.movX + b1.movY*b1.movY );
//				v2sp = Math.sqrt( b2.movX*b2.movX + b2.movY*b2.movY );

				itx = -iny;
				ity = inx;

				vn1sp = b1.movX*inx + b1.movY*iny;
				vn2sp = b2.movX*inx + b2.movY*iny;

				vt1sp = b1.movX*itx + b1.movY*ity;
				vt1x = itx * vt1sp;
				vt1y = ity * vt1sp;
				vt2sp = b2.movX*itx + b2.movY*ity;
				vt2x = itx * vt2sp;
				vt2y = ity * vt2sp;

				outvn1sp = (m2*vn2sp*(1+coefBall) + vn1sp*(m1-(coefBall*m2)))/(m1+m2);
				outvn1x = outvn1sp * inx;
				outvn1y = outvn1sp * iny;
				outvn2sp = (m1*vn1sp*(1+coefBall) + vn2sp*(m2-(coefBall*m1)))/(m2+m1);
				outvn2x = outvn2sp * inx;
				outvn2y = outvn2sp * iny;

				outv1x = outvn1x + vt1x;
				outv1y = outvn1y + vt1y;
				outv2x = outvn2x + vt2x;
				outv2y = outvn2y + vt2y;

				b1.setMovXY(outv1x, outv1y);
				b2.setMovXY(outv2x, outv2y);
			}
		}
	}
	void breakBall(int i, double bulX, double bulY){
		//ボール分裂処理
		MyBall mb = list.get(i);
		if(mb.size > 5){
			double dir = Math.atan2((mb.y-bulY),(mb.x-bulX));
			double speed = 10;
			addBallDir(mb.x, mb.y, mb.size/2, dir-0.2, speed, mb.color);
			addBallDir(mb.x, mb.y, mb.size/2, dir+0.2, speed, mb.color);
		}
		list.get(i).setDelFlag();
	}
	void delBall(){
		for(i=getBallNum()-1; i>=0; i--){
			if(list.get(i).getDelFlag()){
				list.remove(i);
			}
		}
	}
	void delBallAll(){
		for(i=getBallNum()-1; i>=0; i--){
			list.remove(i);
		}
	}
}