import java.awt.Graphics;
import java.awt.Color;
import java.util.*;

class Bullet{
	Stage stage;
	double width, height;
	double x, y;
	double sizeX, sizeY;
	double speed;
	double dir; // 弾が進む角度
	Color color;
	boolean delFlag = false;
	Bullet(Stage st, double x, double y, double sx, double sy, double sp, double d){
		stage = st;
		width = stage.getWidth();
		height = stage.getHeight();
		this.x = x;
		this.y = y;
		sizeX = sx;
		sizeY = sy;
		speed = sp;
		dir = d;
		color = Color.green;
	}
	double getCenterX(){return (x + sizeX/2);}
	double getCenterY(){return (y + sizeY/2);}
	boolean getDelFlag(){return delFlag;}
	void setDelFlag(){delFlag = true;}
	void draw(Graphics g){
		Color backup = g.getColor();
		g.setColor(color);
		g.fillRect((int)x, (int)y, (int)sizeX, (int)sizeY );
		g.setColor(backup);
	}
	void next(){
		x += Math.cos(dir) * speed;
		y += Math.sin(dir) * speed;
	}
}
class OnoreBullet extends Bullet{
	OnoreBullet(Stage st, double x, double y){
		super(st,x-7,y,14,45,10,Math.PI*1.5);
	}
}
class ListOnoreBullet{
	List<OnoreBullet> list;
	Stage stage;
	double width, height;
	int i;
	ListOnoreBullet(Stage st){
		stage = st;
		width = st.getWidth();
		height = st.getHeight();
		list = new ArrayList<OnoreBullet>();
	}
	public int getBulNum(){ return list.size(); }
	void shot(double x, double y){
		list.add(new OnoreBullet(stage, x, y));
	}
	void draw(Graphics g){
		for (i=list.size()-1; i>=0; i--){
			list.get(i).draw(g);
		}
	}
	void next(){
		deleteBul();
		for (i=list.size()-1; i>=0; i--){
			list.get(i).next();
		}
	}
	void deleteBul(){
		for (i=list.size()-1; i>=0; i--){
			if (list.get(i).getDelFlag()){
				list.remove(i);
			}
			else if (list.get(i).x + list.get(i).sizeX < 0 || width < list.get(i).x || list.get(i).y + list.get(i).sizeY < 0 || height < list.get(i).y) {
				list.remove(i);
			}
		}
	}
	void deleteBulAll(){
		for(i=list.size()-1; i>=0; i--){
			list.remove(i);
		}
	}
	void hitBullet(int i){
		list.remove(i);
	}
}